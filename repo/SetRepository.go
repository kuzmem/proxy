package repo

import (
	"context"
	"errors"
	"fmt"
	pb "gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"google.golang.org/protobuf/types/known/emptypb"
	"sync"
)

type Repoer interface {
	GetAllDashboardIn()
	GetPriceChangeIn()
	GetAllDashboard(ctx context.Context, in *emptypb.Empty) (*pb.GetAllDashboardOut, error)
	GetDashboardsBySymbols(ctx context.Context, in *pb.GetDashboardsBySymbolsIn) (*pb.GetDashboardsBySymbolsOut, error)
	GetPriceChangeBySymbol(ctx context.Context, in *pb.GetDashboardIn) (*pb.PriceChangeStats, error)
	GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*pb.GetPriceChangeOut, error)
	GetDashboard(ctx context.Context, in *pb.GetDashboardIn) (*pb.DashboardPeriods, error)
}

type Repo struct {
	Client client.WarehouseClient
	Cache  map[string]interface{}
	mu     *sync.Mutex
}

func NewRepo(Client client.WarehouseClient) *Repo {
	return &Repo{
		Client: Client,
		Cache:  make(map[string]interface{}),
		mu:     &sync.Mutex{},
	}
}

func (r *Repo) GetAllDashboardIn() {
	r.mu.Lock()
	defer r.mu.Unlock()
	getAll := r.Client.GetAllDashboard()
	r.Cache["GetAll"] = getAll
}

func (r *Repo) GetPriceChangeIn() {
	r.mu.Lock()
	defer r.mu.Unlock()
	Price := r.Client.GetPriceChange()
	r.Cache["GetPrice"] = Price
}

func (r *Repo) GetAllDashboard(ctx context.Context, in *emptypb.Empty) (*pb.GetAllDashboardOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		return nil, errors.New("cant get data from GetAll")
	}
	mapData, ok := data.(map[string]interface{})
	if !ok {
		fmt.Println("cant convert to map")
	}
	periods := make([]*pb.DashboardPeriods, 203)
	for key, value := range mapData {
		var period *pb.DashboardPeriods
		if !ok {
			fmt.Println(fmt.Sprintf("cant get periods for %s", key))
		}
		newdashboard, ok := value.(map[string]*pb.Dashboard)
		if !ok {
			fmt.Println(fmt.Sprintf("cant convert periods for %s", key))
		}
		period = &pb.DashboardPeriods{
			Symbol:  key,
			Periods: newdashboard,
		}

		periods = append(periods, period)
	}
	response := pb.GetAllDashboardOut{
		Dashboard: periods,
	}

	return &response, nil
}

func (r *Repo) GetDashboardsBySymbols(ctx context.Context, in *pb.GetDashboardsBySymbolsIn) (*pb.GetDashboardsBySymbolsOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		fmt.Println("cant get data from GetAll")
	}
	mapData, ok := data.(map[string]interface{})
	if !ok {
		fmt.Println("cant convert to map")
	}
	periods := make([]*pb.DashboardPeriods, 203)
	for _, symbol := range in.Symbols {
		var period *pb.DashboardPeriods
		dashboard, ok := mapData[symbol]
		if !ok {
			fmt.Println(fmt.Sprintf("cant get periods for %s", symbol))
		}
		newdashboard, ok := dashboard.(map[string]*pb.Dashboard)
		if !ok {
			fmt.Println(fmt.Sprintf("cant convert periods for %s", symbol))
		}
		period = &pb.DashboardPeriods{
			Symbol:  symbol,
			Periods: newdashboard,
		}

		periods = append(periods, period)
	}
	response := pb.GetDashboardsBySymbolsOut{
		Dashboard: periods,
	}

	return &response, nil
}

func (r *Repo) GetPriceChangeBySymbol(ctx context.Context, in *pb.GetDashboardIn) (*pb.PriceChangeStats, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetPrice"]
	if !ok {
		return nil, errors.New("cant get data from GetPrice")
	}
	sliceData, ok := data.([]*pb.PriceChangeStats)
	if !ok {
		return nil, errors.New("cant convert data in GetPrice")
	}
	for i := range sliceData {
		if sliceData[i].Symbol == in.Symbol {
			return sliceData[i], nil
		}
	}
	return nil, errors.New("cant find price change")
}

func (r *Repo) GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*pb.GetPriceChangeOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetPrice"]
	if !ok {
		return nil, errors.New("cant get data from GetPriceChange")
	}
	stats, ok := data.([]*pb.PriceChangeStats)
	if !ok {
		fmt.Println("cant convert to slice")
	}

	response := pb.GetPriceChangeOut{PriceChange: stats}
	return &response, nil
}

func (r *Repo) GetDashboard(ctx context.Context, in *pb.GetDashboardIn) (*pb.DashboardPeriods, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		return nil, errors.New("cant get data from GetDashboard")
	}
	mapData, ok := data.(map[string]interface{})
	if !ok {
		return nil, errors.New("cant convert to map")
	}
	period, ok := mapData[in.Symbol]
	if !ok {
		return nil, errors.New("cant get period")
	}
	mapPeriod, ok := period.(map[string]*pb.Dashboard)
	response := pb.DashboardPeriods{
		Symbol:  in.Symbol,
		Periods: mapPeriod,
	}
	return &response, nil
}
