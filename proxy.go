package main

import (
	"fmt"
	"github.com/joho/godotenv"
	pb "gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"gitlab.com/kuzmem/proxy/repo"
	"gitlab.com/kuzmem/proxy/service"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"time"
)

type Proxy struct {
	storage service.Servicer
	pb.UnimplementedWarehouseServer
}

func main() {
	//warehouseConn, err := grpc.Dial("79.143.30.74:4080", grpc.WithInsecure())
	//if err != nil {
	//	log.Fatalf("Failed to connect to Warehouse service: %v", err)
	//}
	//defer warehouseConn.Close()
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	addr := os.Getenv("SERVER_IP")
	port := os.Getenv("SERVER_PORT")
	ClAddr := os.Getenv("CLIENT_ADDR")
	warehouseClient := client.NewWarehouseClient(fmt.Sprintf("%s:%s", addr, port), 21*time.Hour)
	repository := repo.NewRepo(*warehouseClient)
	service := service.NewService(*repository)
	proxy := &Proxy{storage: service}
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", ClAddr))
	if err != nil {
		log.Fatal(err)
	}
	s := grpc.NewServer(grpc.MaxRecvMsgSize(1024*1024*1024), grpc.MaxSendMsgSize(1024*1024*1024))
	pb.RegisterWarehouseServer(s, proxy)

	go proxy.GetData()

	if err != nil {
		fmt.Println(err)
	}
	log.Printf("grpc server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatal(err)
	}

}

func (p *Proxy) GetData() {
	ticker := time.NewTicker(time.Minute)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			p.storage.GetAll()
		}
	}
}
