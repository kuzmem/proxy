package service

import (
	"context"
	pb "gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/kuzmem/proxy/repo"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Servicer interface {
	GetAll()
	GetAllDashboard(ctx context.Context, in *emptypb.Empty) (*pb.GetAllDashboardOut, error)
	GetDashboardsBySymbols(ctx context.Context, in *pb.GetDashboardsBySymbolsIn) (*pb.GetDashboardsBySymbolsOut, error)
	GetPriceChangeBySymbol(ctx context.Context, in *pb.GetDashboardIn) (*pb.PriceChangeStats, error)
	GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*pb.GetPriceChangeOut, error)
	GetDashboard(ctx context.Context, in *pb.GetDashboardIn) (*pb.DashboardPeriods, error)
}

type Service struct {
	storage repo.Repo
	pb.UnimplementedWarehouseServer
}

func NewService(repo repo.Repo) *Service {
	return &Service{storage: repo}
}

func (s *Service) GetAll() {
	s.storage.GetAllDashboardIn()
	s.storage.GetPriceChangeIn()
}

func (s *Service) GetAllDashboard(ctx context.Context, empty *emptypb.Empty) (*pb.GetAllDashboardOut, error) {
	return s.storage.GetAllDashboard(ctx, empty)
}

func (s *Service) GetDashboardsBySymbols(ctx context.Context, in *pb.GetDashboardsBySymbolsIn) (*pb.GetDashboardsBySymbolsOut, error) {
	return s.storage.GetDashboardsBySymbols(ctx, in)
}
func (s *Service) GetPriceChangeBySymbol(ctx context.Context, in *pb.GetDashboardIn) (*pb.PriceChangeStats, error) {
	return s.storage.GetPriceChangeBySymbol(ctx, in)
}

func (s *Service) GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*pb.GetPriceChangeOut, error) {
	return s.storage.GetPriceChange(ctx, empty)
}
func (s *Service) GetDashboard(ctx context.Context, in *pb.GetDashboardIn) (*pb.DashboardPeriods, error) {
	return s.storage.GetDashboard(ctx, in)
}
